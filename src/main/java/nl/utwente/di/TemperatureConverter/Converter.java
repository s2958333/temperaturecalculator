package nl.utwente.di.TemperatureConverter;

public class Converter {

    public Converter() {
    }
    public double getFahrenheit(String value) {
        return (Integer.parseInt(value) * 9/5) + 32;
    }
}
